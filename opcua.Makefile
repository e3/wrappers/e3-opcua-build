#
#  Copyright (c) 2018 - 2019  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
#
# Author  : Karl Vestin
# email   : karl.vestin@ess.eu
# Date    : 2020-11-10
# version : 1.0.0
#

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


include $(where_am_I)/configure/CONFIG_OPCUA_VERSION


## Exclude linux-ppc64e6500
EXCLUDE_ARCHS = linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky

OPCUASRC:=devOpcuaSup
UASDKSRC:=$(OPCUASRC)/UaSdk
#EXAMPLEDB:=exampleTop/TemplateDbSup/AnyServerDb


USR_INCLUDES += -I$(COMMON_DIR)
USR_INCLUDES += -I$(where_am_I)$(OPCUASRC)
USR_INCLUDES += -I$(where_am_I)$(UASDKSRC)


### In order to use the same codes in RULES_OPCUA
UASDK:=$(WITH_UASDK)
UASDK_USE_XMLPARSER:=$(WITH_UASDK_USE_XMLPARSER)
UASDK_USE_CRYPTO:=$(WITH_UASDK_USE_CRYPTO)


### START ### RULES_OPCUA in devOpcuaSup/UaSdk
UASDK_MODULES = uaclient uapki uabase uastack
ifeq ($(UASDK_USE_XMLPARSER),YES)
UASDK_MODULES += xmlparser
USR_SYS_LIBS_Linux += xml2
endif
ifeq ($(UASDK_USE_CRYPTO),YES)
USR_SYS_LIBS += crypto
#USR_CXXFLAGS += -DHAS_SECURITY
endif

# Depending on SDK version, C++ modules may have a 'cpp' suffix
UASDK_LIBS = $(notdir $(wildcard $(foreach module, $(UASDK_MODULES), \
     $(UASDK)/include/$(module) $(UASDK)/include/$(module)cpp)))

USR_INCLUDES += $(foreach lib, $(UASDK_LIBS), -I$(UASDK)/include/$(lib))
#
# e3 only supports INSTALL in $(UASDK_DEPLOY_MODE)
# it is not necessary to implement them.
#
### END ### RULES_OPCUA


ifeq (linux-x86_64, $(findstring linux-x86_64,$(T_A)))
USR_LDFLAGS += -Wl,--enable-new-dtags
USR_LDFLAGS += -L"$(UASDK)/lib"
#USR_LDFLAGS += -Wl,-rpath,"\$$ORIGIN/../../../../../siteLibs/vendor/$(E3_MODULE_NAME)/$(E3_MODULE_VERSION)"
LIB_SYS_LIBS += $(UASDK_LIBS)
endif


VENDOR_LIBS += $(foreach lib, $(UASDK_LIBS), $(UASDK)/lib/lib$(lib).so)


# # Generic sources and interfaces
# BEGIN : $(UASDKSRC)/Makefile
DBDS    += $(UASDKSRC)/opcua.dbd
# opcua.dbd has
#               opcuaItemRecord.dbd
#               devOpcua.dbd
#               opcuaUaSdk.dbd
#
SOURCES += $(UASDKSRC)/Session.cpp
SOURCES += $(UASDKSRC)/SessionUaSdk.cpp
SOURCES += $(UASDKSRC)/Subscription.cpp
SOURCES += $(UASDKSRC)/SubscriptionUaSdk.cpp
SOURCES += $(UASDKSRC)/ItemUaSdk.cpp
SOURCES += $(UASDKSRC)/DataElementUaSdk.cpp
# END   : $(UASDKSRC)/Makefile


# BEGIN : $(OPCUASRC)/Makefile
SOURCES += $(OPCUASRC)/devOpcua.cpp
SOURCES += $(OPCUASRC)/iocshIntegration.cpp
SOURCES += $(OPCUASRC)/RecordConnector.cpp
SOURCES += $(OPCUASRC)/linkParser.cpp
SOURCES += $(OPCUASRC)/opcuaItemRecord.cpp
#
# devOpcua.dbd is COMMON_ASSEMBELIES, and
# generated within COMMON. So, e3 needs the ugly
# method to generate the file
#
DBDS    += $(COMMON_DIR)/devOpcua.dbd
DBDS    += $(OPCUASRC)/opcuaItemRecord.dbd

HEADERS += $(OPCUASRC)/devOpcuaVersion.h
HEADERS += $(COMMON_DIR)/opcuaItemRecord.h
HEADERS += $(COMMON_DIR)/devOpcuaVersionNum.h
HEADERS += $(COMMON_DIR)/menuDefAction.h

## Need to define the absolute path, because driver.Makefile
## doesn't know where these files are.
##

devOpcua.dbd_SNIPPETS += $(where_am_I)$(OPCUASRC)/10_devOpcuaInt64.dbd
devOpcua.dbd_SNIPPETS += $(where_am_I)$(OPCUASRC)/20_devOpcuaAll.dbd

opcuaItemRecord$(DEP): $(COMMON_DIR)/devOpcuaVersionNum.h $(COMMON_DIR)/opcuaItemRecord.h $(COMMON_DIR)/devOpcua.dbd

# Module versioning
EXPANDVARS  += EPICS_OPCUA_MAJOR_VERSION
EXPANDVARS  += EPICS_OPCUA_MINOR_VERSION
EXPANDVARS  += EPICS_OPCUA_MAINTENANCE_VERSION
EXPANDVARS  += EPICS_OPCUA_DEVELOPMENT_FLAG
EXPANDFLAGS += $(foreach var,$(EXPANDVARS),-D$(var)="$(strip $($(var)))")

$(COMMON_DIR)/devOpcuaVersionNum.h: $(where_am_I)$(OPCUASRC)/devOpcuaVersionNum.h@
	$(EXPAND_TOOL) $(EXPANDFLAGS) $($@_EXPANDFLAGS) $< $@

$(COMMON_DIR)/opcuaItemRecord.h: $(where_am_I)$(OPCUASRC)/opcuaItemRecord.dbd $(COMMON_DIR)/menuDefAction.h
	$(DBTORECORDTYPEH) $(USR_DBDFLAGS) -o $@ $<

$(COMMON_DIR)/menuDefAction.dbd:
	perl -CSD $(EPICS_BASE)/bin/linux-x86_64/podRemove.pl -o $(COMMON_DIR)/menuDefAction.dbd $(where_am_I)$(OPCUASRC)/menuDefAction.dbd.pod

$(COMMON_DIR)/menuDefAction.h: $(COMMON_DIR)/menuDefAction.dbd
	perl -CSD $(EPICS_BASE)/bin/linux-x86_64/dbdToMenuH.pl -o $(COMMON_DIR)/menuDefAction.h $(COMMON_DIR)/menuDefAction.dbd
#	perl -CSD /epics/base-7.0.4//bin/linux-x86_64/dbdToMenuH.pl   -I. -I.. -I../O.Common -I$(where_am_I)$(OPCUASRC) -I../../dbd -I/epics/base-7.0.4//dbd -o menuDefAction.h $(COMMON_DIR)/menuDefAction.dbd

$(COMMON_DIR)/devOpcua.dbd: $(devOpcua.dbd_SNIPPETS)
	$(ASSEMBLE_TOOL) -o $@ $^

# END : $(OPCUASRC)/Makefile


.PHONY: vlibs
vlibs:
