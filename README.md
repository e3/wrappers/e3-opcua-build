
e3-opcua-build
======
ESS Site-specific EPICS module : opcua (build stage)

> **NOTE**: This module should _not_ have the usual CI/CD pipeline associated with it. It must be built manually.

Due to licensing restrictions from United Automation, `opcua` can only be built by a UA license holder.
As a consequence, we have split the build process into two stages, contained in this wrapper and in a secondary
one [e3-opcua](https://gitlab.esss.lu.se/e3/common/e3-opcua). Please see the build instructions there.
